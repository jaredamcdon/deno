import { signal } from "https://deno.land/std@0.133.0/signal/mod.ts";

console.log("Script awaiting Ctrl+C")

const sig = signal("SIGUSR1", "SIGINT");

// add timeout to prevent process exiting immediately
setTimeout(() => {}, 5000);

for await (const _ of sig) {
	console.log("interrupt of usr1 signal received\nThanks for Ctrl+C'ing!");
	Deno.exit();
}
