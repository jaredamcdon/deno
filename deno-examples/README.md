# Deno Examples
> examples from the Deno Manual

## Need help?
 - [https://doc.deno.land/](https://doc.deno.land/) 
 - just paste in the import URL for documentation

## Running Deno?
 - install with cargo:
   - `cargo install deno --locked`
 - run an app:
   - `deno run filename.ts/js`
 - start language server:
   - `deno lsp`
 - Compile an app (experimental):
   - `deno compille filename.ts/js`

## Examples
 - [Official Documentation](https://deno.land/manual@v1.20.4/introduction)

### cat.ts
 - simple application that works like gnu cat

### file_system_events.ts
 - script that watches for file changes in local directory

### hello.ts
 - prints hello 

### http_req.ts
 - simple curl like application

### math.ts
> TODO
 - lots of math functions for reference

### OS signal handling
 - os_signals_1.ts:
   - handle os signals such as `CTRL+C`
 - os_signals_2.ts:
   - aysnc version of os_signals_1.ts

### simple http_1.ts
 - quick and dirty http server

### simple http_2.ts
 - cleaner and different implementation of http_1.ts

### subprocess.ts
 - subprocess_1.ts:
   - create and run subprocess (command line args) in deno
   - this does not run in a secure sandbox (obviously)
 - subprocess_2.ts:
   - runs sub_process1.ts
 - subprocess_3.ts:
   - runs bash command out outputs to a file

### tcp_server.ts
 - simple tcp server that echos inputs back to client

### types.ts
> TODO
 - all the types, vars, etc of typescript in deno
