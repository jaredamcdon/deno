import { serve } from "https://deno.land/std@0.133.0/http/server.ts";

const address = '0.0.0.0'
const port = 8080

const handler = (request: Request): Response => {
	let body = "Your user-agent is:\n\n";
	body += request.headers.get("user-agent") || "Uknown";

	return new Response(body, { status: 200 });
};

console.log(`HTTP webserver running, Access it at: http://${address}:${port}`);

await serve(handler, { port });
