import { copy } from "https://deno.land/std@0.133.0/streams/conversion.ts";

const hostname = "0.0.0.0";
const port = 7070;
const listener = Deno.listen({ hostname, port });

console.log(`Listening on ${hostname}:${port}`);
console.log(`connect with: nc ${hostname} ${port}`);

for await (const conn of listener) {
	copy(conn, conn);
}
