const serverPort = 8080;
const serverAddress = '0.0.0.0';


const server = Deno.listen({hostname: serverAddress,  port: serverPort });

console.log(`HTTP webserver running. Access it at http://${serverAddress}:${serverPort}`);

for await (const conn of server) {
	serveHttp(conn);
}

async function serveHttp(conn: Deno.Conn) {
	//this converts simple TCP to HTTP
	const httpConn = Deno.serveHttp(conn);
	
	for await (const requestEvent of httpConn) {
	//native deno http uses `Request` and `Response`
		const body = `Your user-agent is:\n\n${
			requestEvent.request.headers.get(
				"user-agent",
			) ?? "Uknown"
		}`;
		requestEvent.respondWith(
			new Response(body, {
				status: 200,
			}),
		);
	}
}
