console.log("Press Ctrl+C to trigger a SIGINT signal");

const sigIntHandler = () => {
	console.log("Signal interupted!");
	Deno.exit(0);
};

Deno.addSignalListener("SIGINT", sigIntHandler);

// add timeout to prevent process existing immediately
setTimeout(() => {}, 5000);

// stop listening for signal after 1s
setTimeout(() => {
	Deno.removeSignalListener("SIGINT", sigIntHandler);
}, 1000);
