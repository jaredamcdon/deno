if ( Deno.args[0] == null ) {
	console.log("Missing command line argument.\nPlease provide a file.");
	Deno.exit(1);
}

const fileNames = Deno.args;

const p = Deno.run({
	cmd: [
		"deno",
		"run",
		"--allow-read",
		"./cat.ts",
		...fileNames
	],
	stdout : "piped",
	stderr : "piped",
});

const  { code } = await p.status();

//reading the outputs closes their pipes
const rawOutput = await p.output();
const rawError = await p.stderrOutput();

if (code === 0) {
	await Deno.stdout.write(rawOutput);
}
else {
	const errorString = new TextDecoder().decode(rawError);
	console.log(errorString);
}

Deno.exit(code);
